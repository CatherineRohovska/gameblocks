//
//  Block.h
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Block : NSObject
@property (nonatomic, readonly) NSInteger number;
@property (nonatomic) bool blocked;
-(instancetype) initWithNumber: (NSInteger) number;
@end
