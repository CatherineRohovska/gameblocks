//
//  Block.m
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "Block.h"

@implementation Block
-(instancetype)initWithNumber:(NSInteger)number
{
    self = [super init];
    if (self) {
        _number  = number;
        _blocked = NO;
    }
    return self;
}
@end
