//
//  BlockCollectionFlowLayout.h
//  BlockPairs
//
//  Created by User on 11/19/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
static const int frameBorder = 3;
@interface BlockCollectionFlowLayout : UICollectionViewLayout

@property (nonatomic) NSInteger cellSide;
@property (nonatomic) UIEdgeInsets insets;
@property (nonatomic) NSInteger cellsRow;
@property (nonatomic) NSInteger cellsColumn;
@end
