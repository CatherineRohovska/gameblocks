//
//  BlockCollectionFlowLayout.m
//  BlockPairs
//
//  Created by User on 11/19/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "BlockCollectionFlowLayout.h"

@implementation BlockCollectionFlowLayout


-(id)init {
    if(self = [super init]) {
        self.insets = UIEdgeInsetsMake(3, 3, 3, 3);
        
        
    }
    return self;
}

-(UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
     UICollectionViewLayoutAttributes *attr = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    
    attr.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.2, 0.2), M_PI);
    attr.center = CGPointMake(CGRectGetMidX(self.collectionView.bounds), CGRectGetMidY(self.collectionView.bounds));
    
    return attr;
}
-(CGSize)collectionViewContentSize
{
   return self.collectionView.bounds.size;
}
-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    
    for (NSInteger i = 0; i < [self.collectionView numberOfItemsInSection:0]; i++)
    {
        [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    
    NSMutableArray *layoutAttributes = [NSMutableArray array];
    
    for (NSIndexPath *indexPath in indexPaths)
    {
        [layoutAttributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
    }
//

    NSArray *answer = [layoutAttributes copy];
    
    for(int i = 1; i < [answer count]; ++i) {
        UICollectionViewLayoutAttributes *currentLayoutAttributes = answer[i];
    
        CGRect frame = currentLayoutAttributes.frame;
    
        NSIndexPath* indexPathCurrent = [indexPaths objectAtIndex:i];
      
        NSInteger cellsInRow = _cellsColumn;//(self.collectionView.bounds.size.width) / _cellSide;
        NSInteger cellsInColumn = _cellsRow;//(self.collectionView.bounds.size.height) / _cellSide;
        if(_cellsRow!=0 && _cellsColumn!=0)
        {
        CGFloat maximumSpacingRow =(self.collectionView.bounds.size.width-2*frameBorder-cellsInRow*_cellSide)/(cellsInRow-1);
        CGFloat maximumSpacingColumn =(self.collectionView.bounds.size.height-2*frameBorder-cellsInColumn*_cellSide)/(cellsInColumn-1);
        
        NSInteger rowCurrent = indexPathCurrent.row / cellsInRow;
        NSInteger columnCurrent = indexPathCurrent.row % cellsInRow;
  
        frame.origin.x = (_cellSide+maximumSpacingRow)*columnCurrent+(CGFloat)frameBorder;
         frame.origin.y = (_cellSide+maximumSpacingColumn)*rowCurrent+(CGFloat)frameBorder;
            currentLayoutAttributes.frame = frame;
        }
    //currentLayoutAttributes.frame = UIEdgeInsetsInsetRect(frame, _insets);
    }
    return answer;
   
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    NSInteger cellsCount = [self.collectionView numberOfItemsInSection:0];
   // _cellSide = 100;
    
    switch (cellsCount)
    {
        default:
        {
            NSInteger pathRow = indexPath.row;
            
            NSInteger cellsInRow = self.collectionView.bounds.size.width / _cellSide;
            
            NSInteger row = pathRow / cellsInRow;
            NSInteger col = pathRow % cellsInRow;
            
            
            attributes.frame = CGRectMake(col+frameBorder,
                                          row+frameBorder,
                                          _cellSide-1,
                                          _cellSide-1);
    
        }
            break;
    }
    return attributes;

}


@end
