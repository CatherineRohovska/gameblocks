//
//  BlockCollectionViewCell.h
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Block.h"
@interface BlockCollectionViewCell : UICollectionViewCell
@property(nonatomic, strong) Block* block;
@property(nonatomic, strong) UILabel* textLabel;
@property(nonatomic) bool openState;
- (void) createWithBlock: (Block*) block;
- (void) changeState;
@end
