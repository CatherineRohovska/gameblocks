//
//  BlockCollectionViewCell.m
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "BlockCollectionViewCell.h"

@implementation BlockCollectionViewCell
{
    UIView* coverView;
    // bool openState;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.backgroundColor = [UIColor blueColor];
        _textLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight);
        [self.contentView addSubview:_textLabel];
        
        coverView = [[UIView alloc] initWithFrame:self.bounds];
        coverView.autoresizingMask = (UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight);
        coverView.backgroundColor = [UIColor magentaColor];
       // [self.contentView addSubview:coverView];
        self.backgroundColor = self.superview.backgroundColor;
        self.hidden = NO;
      
    }
    return self;
}
-(void) recoverCell
{
    [self changeState];
    self.hidden = NO;
  
}
- (void) changeState
{
  
    if (_block.blocked) {
       [self animatedDissapearence];
      

    }
   
        if (!_openState&&!_block.blocked) {
            self.userInteractionEnabled = NO;
            [UIView transitionFromView:coverView toView: _textLabel duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            completion:^(BOOL finished) {
                                self.userInteractionEnabled = YES;
                            }];
            _openState = YES;
        }
        else if (_openState&&!_block.blocked){
            self.userInteractionEnabled = NO;
            [UIView transitionFromView:_textLabel toView: coverView duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromRight
                            completion:^(BOOL finished) {
                                self.userInteractionEnabled = YES;
                                
                            }];
            _openState = NO;
        }
   
    self.layer.borderColor = nil;
    self.layer.borderWidth = 0;
}
-(void) animatedDissapearence
{
    self.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.5 delay:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        self.hidden = YES;
        
        
    }];
}

- (void) createWithBlock: (Block*) block;
{
    _openState = NO;
    _block = block;
    _block.blocked = NO;
    self.hidden = NO;
    self.layer.borderColor = nil;
    self.layer.borderWidth = 0;
    [_textLabel setText:[ @(block.number) stringValue]];
 
    [self.contentView addSubview:coverView];
   
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (!_block.blocked)
    {
        [super touchesBegan:touches withEvent:event];
        
    }
    
}
@end
