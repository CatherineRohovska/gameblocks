//
//  BlockManager.h
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Block.h"
@interface BlockManager : NSObject
@property (nonatomic, readonly) NSMutableArray* blocksCollection;
//+ (id) sharedManager;
-(void)createLevel: (NSInteger) levelNumber;
//-(BOOL)compareBlocks: (NSInteger) blockNumber1 blockNumber2: (NSInteger) blockNumber2;
-(BOOL)compareBlocks: (Block*) block1 block2: (Block*) block2;
-(BOOL) isAllBlocked;
@end
@interface NSMutableArray (Shuffling)
- (void)shuffle;
@end