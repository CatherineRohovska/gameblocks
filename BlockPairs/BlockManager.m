//
//  BlockManager.m
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "BlockManager.h"
static const NSInteger itemsAddPerLevel = 2;
static const NSInteger itemsStart = 8;

@implementation BlockManager
+ (id)sharedManager {
    static BlockManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init ];
    });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _blocksCollection = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)createLevel: (NSInteger) levelNumber
{
    _blocksCollection = [[NSMutableArray alloc] init];
    NSInteger itemsCount = itemsStart+itemsAddPerLevel*levelNumber;
    for (NSInteger i=0; i<itemsCount; i++) {
        Block* singleBlock = [[Block alloc] initWithNumber:i];
        [_blocksCollection addObject:singleBlock];
        [_blocksCollection addObject:singleBlock];
    }
    [_blocksCollection shuffle];
}


-(BOOL)compareBlocks: (Block*) block1 block2: (Block*) block2
{
    BOOL result = NO;
    
    if (block1.number==block2.number)
    {
        result = YES;
    
        block1.blocked = block2.blocked = YES;
    }
    return result;
}
-(BOOL) isAllBlocked
{
    BOOL res = NO;
    NSInteger blockedCount = 0;
    for (Block* singleBlock in _blocksCollection) {
        if(singleBlock.blocked) {blockedCount++;}
        
    }
    if (blockedCount==_blocksCollection.count) {
        res=YES;
    }
    return res;
}
@end

@implementation NSMutableArray (Shuffling)

- (void)shuffle
{
    NSUInteger count = [self count];
    for (NSUInteger i = 0; i < count - 1; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [self exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

@end
