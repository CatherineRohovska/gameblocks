//
//  BlockPairsGameManager.h
//  BlockPairs
//
//  Created by User on 11/17/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlockManager.h"
static const NSInteger maxLevel = 20;
@class BlockPairsGameManager;
@protocol BlockPairsGameManagerDelegate <NSObject>

@required
-(void) changeLevel: (NSInteger) level; //performs while level changing
@required
-(void) changeTimer: (NSInteger) currentTime; //timer changes its own value
@required
-(void) actionOnHint; // action when hint appears
@required
-(void) gameAllOver; //  reaches 20 level- the cap of game levels
@end

@interface BlockPairsGameManager : NSObject
@property (nonatomic, readonly) NSInteger currentLevel;
@property (nonatomic, readonly) NSInteger maxLevelReached;
@property (nonatomic, strong, readonly) BlockManager* blockManager;
@property (nonatomic, strong) id<BlockPairsGameManagerDelegate> delegate;
+(id)sharedManager;
-(void) startLevel; //start current level that strores in BlockPairsGameManager
-(void) startLevelWithLevel: (NSInteger) level; //start selected level from game interface
-(void) nextLevel; //go to the next level after current
-(BOOL) compareBlocks: (Block*) block1 block2: (Block*) block2; //comparing blocks
-(void) pause; //pause countdown and appearance  hints
-(void) resume; //resume countdown and hints appearance
@end
