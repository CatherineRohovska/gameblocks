//
//  BlockPairsGameManager.m
//  BlockPairs
//
//  Created by User on 11/17/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "BlockPairsGameManager.h"

static const NSInteger timeToFindPair = 5;
static const NSInteger timeToShowHint = 10;
@implementation BlockPairsGameManager
{
    NSTimer* timer;
    NSInteger currentTime;
    NSInteger lastFindPair;
}
+ (id)sharedManager {
    static BlockPairsGameManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init ];
    });
    return sharedManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
         NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
        if ([defaults objectForKey:@"BlockPairsGame"] )
        {
            _currentLevel  = [[defaults objectForKey:@"BlockPairsGame"] integerValue];
            _currentLevel = 20;
            _maxLevelReached = _currentLevel;
            
        }
        else
        {
            _currentLevel = 1;
            [defaults setObject:[@(_currentLevel) stringValue]  forKey:@"BlockPairsGame"];
        }
        _blockManager = [[BlockManager alloc] init];
        
    }
    return self;
}
-(BOOL) compareBlocks: (Block*) block1 block2: (Block*) block2
{
   bool res = [_blockManager compareBlocks:block1 block2:block2];
        if ([_blockManager isAllBlocked]) {
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"LevelComplete"
                 object:nil];
            lastFindPair = currentTime;
        }
    return res;

}
-(void) startLevel
{
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
   
    [_blockManager createLevel:_currentLevel];
    currentTime = _blockManager.blocksCollection.count*timeToFindPair;
    lastFindPair = currentTime;
    if ([_delegate respondsToSelector:@selector(changeLevel:)]) {
        [_delegate changeLevel: _currentLevel];
    }
    NSLog(@"Blocks count: %lu", (unsigned long)[_blockManager.blocksCollection count]);
}
-(void) timerTick
{
    if (currentTime==0){
        [timer invalidate];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"LevelFailed"
         object:nil];
    }
    
    if (currentTime>0) {
        currentTime--;
        if ([_delegate respondsToSelector:@selector(changeTimer:)]) {
            
            [_delegate changeTimer:currentTime];
        }
    }
   
    if ([_blockManager isAllBlocked]) {
        [timer invalidate];
    }
    if(lastFindPair-currentTime>timeToShowHint)
    {
        lastFindPair = currentTime;
        if ([_delegate respondsToSelector:@selector(actionOnHint)]) {
         
           [_delegate actionOnHint];
        }
    }
   
  //  NSLog(@"timer tick %ld", (long)currentTime);
}
-(void) nextLevel
{
   if(_currentLevel!=maxLevel)
   {
       _currentLevel++;
       if (_currentLevel>_maxLevelReached) {
           _maxLevelReached = _currentLevel;
           NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
           [defaults setObject:[@(_currentLevel) stringValue]  forKey:@"BlockPairsGame"];
       }
      
        if (_currentLevel==maxLevel+1)
        {
            if ([_delegate respondsToSelector:@selector(gameAllOver)])
            {
                   
                [_delegate gameAllOver];
                _currentLevel = maxLevel;
            }
        }
       
 

       [self startLevel];
   }
}

-(void)startLevelWithLevel:(NSInteger)level
{
    _currentLevel = level;
    [self startLevel];
}
-(void)pause
{
    [timer invalidate];
}
-(void) resume
{
    [timer invalidate];
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
}

@end
