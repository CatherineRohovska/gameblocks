//
//  ViewController.h
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockPairsGameManager.h"
@interface ViewController : UIViewController
<UIScrollViewDelegate, BlockPairsGameManagerDelegate, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *blocksViewer;
- (IBAction)restartLevelClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *timerMeter;

@property (weak, nonatomic) IBOutlet UITableView *levelsTable;
- (IBAction)pauseGame:(id)sender;

@end

