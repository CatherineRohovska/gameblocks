//
//  ViewController.m
//  BlockPairs
//
//  Created by User on 11/16/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"

#import "BlockCollectionViewCell.h"
#import "BlockCollectionFlowLayout.h"
static const int tableHeaderHeight = 20;
@interface ViewController ()
{
    BlockPairsGameManager* gameManager; //main game logic object
    NSInteger countSelected; //counts selected items
    NSInteger firstBlock; //index of first cell to compare
    NSInteger secondBlock; //index of second cell to compare
    float cellHeight; //i.e cell width 'cause it's square
    Block* block1; //first block to compare
    Block* block2;// second block to compare
    bool appearenceOnce; //flag that indicate first appearance of view
    bool firstCalledTimer; // timer made it's first tick after creating level (to make countdown)
    NSInteger levelTimeMaxValue; //max value of time in seconds
    BlockCollectionViewCell* cellOnHint; //indicate cell tha currently hints
    NSInteger prevCellsCount; //store previous value of cells count before creating new level
    UIView* viewPause; //view displayed on pause
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appearenceOnce = NO;

    gameManager = [BlockPairsGameManager sharedManager];
    gameManager.delegate= self;
    [gameManager startLevel];
    countSelected = 0;
    firstBlock =-1;
    secondBlock = -1;
    firstCalledTimer = NO;
    block1 = nil;
    block2 = nil;
    prevCellsCount = gameManager.blockManager.blocksCollection.count;
    
    _blocksViewer.backgroundColor = nil;
    
    viewPause = [[UIView alloc] initWithFrame:self.view.bounds];
    viewPause.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.35f];
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    label.text = @"GAME PAUSED";
    label.textColor = [UIColor redColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.center = viewPause.center;
    [viewPause addSubview:label];
    
    UITapGestureRecognizer *twiceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resumeGameClick)];
    twiceTap.numberOfTapsRequired = 1;
    [viewPause setUserInteractionEnabled:YES];
    [viewPause addGestureRecognizer:twiceTap];
    
}

-(void) resumeGameClick
{   [gameManager resume];
    [viewPause removeFromSuperview];
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!appearenceOnce) {
        appearenceOnce = YES;
        [self calculateSizeOfCell];
        UIImage* img = [UIImage imageNamed:@"violet_background"];
        UIImageView* imgView = [[UIImageView alloc] initWithFrame:_blocksViewer.bounds];
        CGRect frame = _blocksViewer.bounds;
        frame.origin.x = _blocksViewer.frame.origin.x;
        frame.origin.y = _blocksViewer.frame.origin.y;
        imgView.frame = frame;
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.image = img;
        
        [self.view insertSubview:imgView belowSubview:_blocksViewer];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(levelCompletion)
                                                 name:@"LevelComplete"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(levelFailed)
                                                 name:@"LevelFailed"
                                               object:nil];
   [gameManager resume];
}

-(void) calculateSizeOfCell
{
    int minSpace = 1;
    int allDim = (_blocksViewer.bounds.size.height-2*frameBorder)*(_blocksViewer.bounds.size.width-2*frameBorder);
    int sizeCell = allDim/[gameManager.blockManager.blocksCollection count];
    
    float dimension = sqrtf(sizeCell);

    cellHeight = floor(dimension);//(_blocksViewer.bounds.size.height-6)/(dimension);
    int countHeight = (_blocksViewer.bounds.size.height-2*frameBorder)/(cellHeight+2*minSpace);
    int countWidth = (_blocksViewer.bounds.size.width-2*frameBorder)/(cellHeight+2*minSpace);

    while (gameManager.blockManager.blocksCollection.count>countHeight*countWidth) {
        cellHeight--;
      countHeight = (_blocksViewer.bounds.size.height-2*frameBorder)/(cellHeight+2*minSpace);
    //  countWidth = (_blocksViewer.bounds.size.width-2*frameBorder)/(cellHeight+2*minSpace);

    }
    
    
    
    BlockCollectionFlowLayout* layout = ( BlockCollectionFlowLayout*)_blocksViewer.collectionViewLayout;
    layout.cellsColumn = countWidth;
    layout.cellsRow = countHeight;
    [self reloadCollection];
    
    

}
- (void) reloadCollection
{
    //current data will be previous after update
     NSMutableArray* pathsArrayPrevious = [[NSMutableArray alloc] init];
    for (NSInteger i=0; i<prevCellsCount; i++) {
         NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:0];
        [pathsArrayPrevious addObject:path];
    }
    
    NSMutableArray* pathsArray = [[NSMutableArray alloc] init];
    for (NSInteger i=0; i<gameManager.blockManager.blocksCollection.count; i++) {
        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:0];
        [pathsArray addObject:path];
    }
    BlockCollectionFlowLayout* layout = ( BlockCollectionFlowLayout*)_blocksViewer.collectionViewLayout;
    layout.cellSide = floor(cellHeight);

    [_blocksViewer performBatchUpdates:^{
   
        [_blocksViewer deleteItemsAtIndexPaths:[pathsArrayPrevious copy]]; //clear previous
        [_blocksViewer insertItemsAtIndexPaths:[pathsArray copy]]; //add new
        
    }
                            completion:^(BOOL finished) {
                                
                                
                            }];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LevelComplete" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LevelFailed" object:nil];
    [gameManager pause];
}
-(void) levelCompletion
{
    [gameManager pause];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Congratulations!"
                                                                   message:@"Level complete!"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         NSLog(@"Level complete!");
                                                         [gameManager nextLevel];
                                                         [self calculateSizeOfCell];
                                                       //  [_blocksViewer reloadData];
                                                          firstCalledTimer = NO;
                                                         [_levelsTable reloadData];
                                                     }];
    
    
    [alert addAction:okAction];
    
 [self presentViewController:alert animated:YES completion:nil];
}
-(void) levelFailed
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                   message:@"You lose."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         NSLog(@"Level failed!");
                                                         firstCalledTimer = NO;
                                                         [gameManager startLevel];
                                                         [self calculateSizeOfCell];
                                                        // [_blocksViewer reloadData];
                                                     }];
    
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    prevCellsCount = gameManager.blockManager.blocksCollection.count;
    return [gameManager.blockManager.blocksCollection count];//counts data to show
}


- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BlockCollectionViewCell* cell = (BlockCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BlockCell" forIndexPath:indexPath];
   
    [cell createWithBlock:[gameManager.blockManager.blocksCollection objectAtIndex:indexPath.item]];
  
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    size.width =ceil(cellHeight);
    size.height =ceil(cellHeight);
    
    return  size;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BlockCollectionViewCell* cell = (BlockCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
   
      countSelected++;

    if (countSelected>2)
    {
        countSelected=0;
        for(BlockCollectionViewCell *cells in collectionView.visibleCells)
        {
            if (cells.openState)
            {
                [cells changeState];
            }
        }
        firstBlock = secondBlock = -1;
         block1= block2=nil;
    }
    else
    {
        if (!block2 && block1 && firstBlock!= indexPath.item) {
            secondBlock = indexPath.item;
            block2 = [gameManager.blockManager.blocksCollection objectAtIndex:indexPath.item];
            
        }
        if (!block1) {
            firstBlock = indexPath.item;
            block1 = [gameManager.blockManager.blocksCollection objectAtIndex:indexPath.item];
        }
       
         [cell changeState];

        if (block1  && block2 && firstBlock!=secondBlock)
        {
            if ([gameManager compareBlocks:block1 block2:block2])
            {
                for(BlockCollectionViewCell *cells in _blocksViewer.visibleCells)
                {
                    if (cells.openState)
                    {
                        [cells changeState];
                    }
                }
               // [collectionView reloadData];
                countSelected =0;
                firstBlock = secondBlock = -1;
                block1= block2=nil;
            }
        }
    }
    
}


- (IBAction)restartLevelClick:(id)sender {
    [gameManager pause];
   
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Restart this level"
                                                                   message:@"Do you really want to restart level?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         // [gameManager resume];
                                                         
                                                         [gameManager startLevel];
                                                         [self reloadCollection];
                                                         countSelected =0;
                                                         firstBlock = secondBlock = -1;
                                                         block1= block2=nil;
                                                         firstCalledTimer = NO;
                                                         
                                                        
                                                     }];
    
    
    [alert addAction:okAction];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                        [gameManager resume];
                                                     }];
     [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
 
 
}
-(void)changeLevel:(NSInteger)level
{
    _levelLabel.text = [NSString stringWithFormat:@"Level: %ld", (long)level];
}
-(void)changeTimer:(NSInteger) currentTime
{
    if (!firstCalledTimer) {
        firstCalledTimer = YES;
        levelTimeMaxValue = currentTime;
       
    }
     _timerMeter.progress = (float)currentTime/(float)levelTimeMaxValue;
}
- (void)actionOnHint
{
    NSInteger randomCellNumber =  arc4random_uniform((u_int32_t )_blocksViewer.visibleCells.count);
  
    cellOnHint = [_blocksViewer.visibleCells objectAtIndex:randomCellNumber];
    while (cellOnHint.hidden||cellOnHint.openState) {
        randomCellNumber =  arc4random_uniform((u_int32_t )_blocksViewer.visibleCells.count);
        cellOnHint = [_blocksViewer.visibleCells objectAtIndex:randomCellNumber];
    }
  
    [cellOnHint changeState];
    cellOnHint.layer.borderColor = [[UIColor redColor] CGColor];
    cellOnHint.layer.borderWidth = 3;
    _blocksViewer.userInteractionEnabled = NO;

    [self performSelector:@selector(coverCellAfterHint:) withObject:cellOnHint afterDelay:1.0];
    
   
}
- (void)gameAllOver
{
    [gameManager pause];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Congratulations!"
                                                                   message:@"You have reached maximum level"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                          [gameManager resume];
                                                         
                                                         
                                                     }];
    
    
    [alert addAction:okAction];
 
    [self presentViewController:alert animated:YES completion:nil];
}
-(void) coverCellAfterHint: (BlockCollectionViewCell*) cell
{
    if (cell.openState) {
     [cell changeState];
        
    }

_blocksViewer.userInteractionEnabled = YES;
}
//tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return gameManager.maxLevelReached;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"levelTableCell" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    cell.textLabel.text = [@(indexPath.item+1) stringValue];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [gameManager startLevelWithLevel:indexPath.item+1];
    [self calculateSizeOfCell];
    firstCalledTimer = NO;
    countSelected =0;
    firstBlock = secondBlock = -1;
    block1= block2=nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return tableHeaderHeight;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableHeaderHeight)];
    label.text = @"Select level";
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    return label;
}


- (IBAction)pauseGame:(id)sender
{
    [gameManager pause];
    [self.view addSubview:viewPause];
}
@end
